import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyBk0xL8y6T0XN9rX1IBQ55NdGISk1uo0zI",
    authDomain: "crwn-db-2ddb7.firebaseapp.com",
    databaseURL: "https://crwn-db-2ddb7.firebaseio.com",
    projectId: "crwn-db-2ddb7",
    storageBucket: "crwn-db-2ddb7.appspot.com",
    messagingSenderId: "1051415688021",
    appId: "1:1051415688021:web:0f8cc0e79b4c07aba735dc"
};

firebase.initializeApp(config);

export const createUserProfileDocument = async (userAuth, additionalData) => {
    if (!userAuth) return;
  
    const userRef = firestore.doc(`users/${userAuth.uid}`);
  
    const snapShot = await userRef.get();
  
    if (!snapShot.exists) {
      const { displayName, email } = userAuth;
      const createdAt = new Date();
      try {
        await userRef.set({
          displayName,
          email,
          createdAt,
          ...additionalData
        });
      } catch (error) {
        console.log('error creating user', error.message);
      }
    }
  
    return userRef;
  };
  
  export const auth = firebase.auth();
  export const firestore = firebase.firestore();
  
  const provider = new firebase.auth.GoogleAuthProvider();
  provider.setCustomParameters({ prompt: 'select_account' });
  export const signInWithGoogle = () => auth.signInWithPopup(provider);
  
  export default firebase;